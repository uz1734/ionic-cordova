import { Component } from '@angular/core';

import { Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

import { FCM } from '@ionic-native/fcm/ngx';
import { NativeAudio } from '@ionic-native/native-audio/ngx';
import { BackgroundMode } from '@ionic-native/background-mode/ngx';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent {
  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private fcm: FCM,
    private nativeAudio: NativeAudio,
    private backgroundMode: BackgroundMode
  ) {
    this.initializeApp();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();

      this.backgroundMode.enable();

      this.nativeAudio.preloadComplex('trackID', 'assets/test.mp3', 1, 1, 0).then( () => {
        console.log('audio loaded!');
      }, (err) => {
        console.log('audio failed: ' + err);
      });

      /*this.backgroundMode.on('activate').subscribe(
          (rsp) => {
            console.log('activating: ', rsp);
          },
          (error) => {
            console.log('error');
          }
      );

      this.backgroundMode.on('deactivate').subscribe(
          (rsp) => {
            this.nativeAudio.play('trackID');
          },
          (error) => {
            console.log('error');
          }
      );*/

      /*this.fcm.getToken().then(token => {
        console.log(token);
      });
      this.fcm.onTokenRefresh().subscribe(token => {
        console.log(token);
      });*/
    });

    this.backgroundMode.isScreenOff((isOff) => {
      console.log('isOff: ', isOff);
    });
  }
}
