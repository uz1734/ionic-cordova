import { Component } from '@angular/core';
import {NativeAudio} from '@ionic-native/native-audio/ngx';
import {BackgroundMode} from '@ionic-native/background-mode/ngx';
import {FCM} from '@ionic-native/fcm/ngx';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

  constructor(    private nativeAudio: NativeAudio,
                  private backgroundMode: BackgroundMode,
                  private fcm: FCM) {}

  public playAudio() {
    this.backgroundMode.enable();
    this.backgroundMode.on('activate').subscribe(() => {
      this.backgroundMode.disableWebViewOptimizations();
      console.log('in background mode');
      // this.nativeAudio.play('trackID');
      this.fcm.onNotification().subscribe(data => {
        console.log('hello: ', data);
        this.nativeAudio.play('trackID').then( () => {
          console.log('playing');
        }, (error) => {
          console.log(error);
        });
        if (data.wasTapped) {
          console.log('Received in background');
        } else {
          console.log('Received in foreground');
        }
      });

    });
    console.log('play audio');
    // this.nativeAudio.play('trackID');


    this.fcm.onNotification().subscribe(data => {
      console.log('hello2: ', data);
      this.nativeAudio.play('trackID').then( () => {
        console.log('playing');
      }, (error) => {
        console.log(error);
      });
      if (data.wasTapped) {
        console.log('Received in background');
      } else {
        console.log('Received in foreground');
      }
    });

  }

}
