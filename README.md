# Ionic cordova test app

POST:  https://fcm.googleapis.com/fcm/send

{
  "notification":{
    "title":"Ionic 4 Notification",
    "body":"This notification sent from POSTMAN using Firebase HTTP protocol",
    "sound":"test.mp3",
    "click_action":"FCM_PLUGIN_ACTIVITY",
    "icon":"fcm_push_icon"
  },
  "data":{
    "landing_page":"second",
    "price":"$3,000.00"
  },
    "to":"cC1pXTw36nA:APA91bHOmyhWrmrUrnorBcWh4dCRws0tdY9QKyy-NrhgtpkSBrw5sM36gbVA4H7f4Ih656Rp_d9uMxrADmDSPM_j51pKJsW-h9BWNvnhL14nN5F8ZroMpVdDpMCJIp1qD5qktb9n-Pcn",
    "priority":"high",
    "restricted_package_name":""
}